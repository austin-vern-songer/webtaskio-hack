

I want a simple app/hack that waits/records everytime that Donald Trump Tweets to have [Twilio](https://www.twilio.com/docs/quickstart/php/sms#faq) send a text message to a phone number.

Must somehow use [webtask.io](http://webtask.io) by sending and saving those results in [Mongolab Sandbox](https://mlab.com/) 

#### Requirements

- [Twitter](http://twitter.com)
- [webtask.io](http://webtask.io)
- [Twilio](https://www.twilio.com/docs/quickstart/php/sms#faq)
- [Mongolab Sandbox](https://mlab.com/)
